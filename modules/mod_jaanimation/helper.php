<?php	 	 	 	 
/*
# ------------------------------------------------------------------------
# JA Animation module for Joomla 1.5.x
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/
  // no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.html.parameter');
jimport('joomla.filesystem.folder');
jimport('joomla.filesystem.file');
/**
 * modJAAnimation class.
 */
class ModJAAnimation {
	
    function &getInstance(){
		static $instance = null;
		if( !$instance ){
			$instance = new ModJAAnimation();
		}
		return $instance;
	}
	public function loadConfig($params,$modulename = "mod_jaanimation")
	{
		global $mainframe;
		$use_cache = $mainframe->getCfg("caching");
		if ( $params->get('cache') == "1" && $use_cache == "1") {
			$cache =& JFactory::getCache();
			$cache->setCaching( true );
			$cache->setLifeTime( $params->get( 'cache_time', 30 ) * 60 );
			$params = $cache->get( array( (new ModJAAnimation() ) , 'loadProfile' ), array( $params,$modulename ) );
		} else {
			$params = ModJAAnimation::loadProfile( $params,$modulename );
		}					
		return $params;
	}
 	public static function loadProfile($params,$modulename = "mod_jaanimation")
 	{
		global $mainframe;
		$profilename	    = $params->get('profile','hallowen2');
		if(!empty($profilename))
 		{
			
 			$path = JPATH_ROOT.DS."modules".DS.$modulename.DS."admin".DS."config.xml";
 			$ini_file = JPATH_ROOT.DS."modules".DS.$modulename.DS."profiles".DS.$profilename.".ini";
 			$config_content = "";
 			if(file_exists($ini_file))
 			{
 				$config_content = JFile::read($ini_file);
 			}
			if(empty($config_content))
			{
				$ini_file_in_temp = JPATH_SITE.DS.'templates'.DS.$mainframe->getTemplate().DS.'html'.DS.$modulename.DS.$profilename.".ini";
				if (is_file($ini_file_in_temp))
					$config_content = JFile::read($ini_file_in_temp);
				else
					$config_content = ModJAAnimation::loadIniDefault();
			}

 			if(file_exists($path) && !empty($config_content))
 			{
 				$params_new = new JParameter( $config_content,$path);
 				return $params_new;
 			}
 		}
 		return $params;
 	}
	public static function loadIniDefault()
	{
		$config_content = "";
		
		return $config_content;
	}
}