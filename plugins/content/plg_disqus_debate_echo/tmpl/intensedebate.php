<?php
/*
# ------------------------------------------------------------------------
# JA Disqus and Debate comment for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/

	//IntenseDebate config
	$account        = $this->plgParams->get('provider-intensdebate-account');
	
	if ($this->isComment == false) {
	$url = str_replace('&amp;', '&', $this->_url );
	?>
    	<span class="containerCountComment" onclick="window.location.href='<?php echo $this->_url;?>#idc-container'; return false;">
		<script type="text/javascript">
		//<![CDATA[
			var idcomments_acct = "<?php echo $account?>";
			var idcomments_post_id = "<?php echo $this->_postid?>";
			var idcomments_post_url = encodeURIComponent("<?php echo $url;?>");
		//]]> 
		</script>
		<script type="text/javascript" src="http://www.intensedebate.com/js/genericLinkWrapperV2.js"></script>
        </span>
	<?php }else{?>
		<script type="text/javascript">
		//<![CDATA[
		var idcomments_acct = "<?php echo $account?>";
		var idcomments_post_id = "<?php echo $this->_postid?>";
		var idcomments_post_url = "<?php echo $this->_url?>";
		//]]> 
		</script>
		<span id="IDCommentsPostTitle" style="display:none"></span>
		<script type="text/javascript" src="http://www.intensedebate.com/js/genericCommentWrapperV2.js"></script>
	<?php } ?>