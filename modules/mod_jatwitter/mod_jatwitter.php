<?php	 	 	 	
/*
# ------------------------------------------------------------------------
# JA Twitter module for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/
// no direct access	
defined ( '_JEXEC' ) or die ( 'Restricted access' );

require_once (dirname ( __FILE__ ) . DS . 'jatwitter.php');
require_once (dirname ( __FILE__ ) . DS . 'helper.php');

// get params.
$is_ajax = $params->get ( 'is_ajax' );
$taccount = $params->get ( 'taccount' );
$show_limit = $params->get ( 'show_limit' );
$headingtext = $params->get ( 'headingtext' );
$showfollowlink = $params->get ( 'showfollowlink' );
$showtextheading = $params->get ( 'showtextheading' );
$displayitem = $params->get ( 'displayitem' );
$showIcon = $params->get ( 'show_icon', 1 );
$showUsername = $params->get ( 'show_username', 1 );
$showSource = $params->get ( 'show_source', 1 );
/*$username = $params->get( 'username' );
$password = $params->get( 'password'); */
$apikey = $params->get ( 'apikey' );
$screenName = $params->get ( 'taccount' );
$layout = $is_ajax ? 'default.ajax' : 'default';
$useDisplayAccount = $params->get ( 'use_display_taccount', 0 );
$useFriends = $params->get ( 'use_friends', 0 );
$iconsize = $params->get ( 'icon_size', 48 );
$sizeIconaccount = $params->get ( 'size_iconaccount', 48 );
$sizeIconfriend = $params->get ( 'size_iconfriend', 24 );

$jatHerlper = new modJaTwitterHelper ();
$jatHerlper->loadFiles ( $params, $module->module );
// render layout 



// enable or disable using cache data
$jatHerlper->setCache ( $params->get ( 'enable_cache' ), $params->get ( 'cache_time' ) );
// if show account information
if ($useDisplayAccount) {
	$accountInfo = $jatHerlper->getTwitter ( 'show', $screenName );
}
if ($useFriends) {
	$friends = $jatHerlper->getTwitter ( 'friends', $screenName, $params->get ( 'max_friends', 10 ) );
}
$list = array ();
if ($params->get ( 'twitter_method' ) != 'none') {
	$list = $jatHerlper->getTwitter ( $params->get ( 'twitter_method' ), $screenName, $show_limit, ( int ) $params->get ( 'tweets_cachetime', 30 ) );
}
$layout = 'default2';

include (JModuleHelper::getLayoutPath ( 'mod_jatwitter', $layout ));
?>
