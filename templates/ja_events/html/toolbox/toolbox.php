<?php	 	 	 	


// no direct access
defined('_JEXEC') or die('Restricted access'); ?>
<?php	 	 	 	 
if(!defined('JA_SOCIAL_TOOLBOX_DISPLAY')):
define('JA_SOCIAL_TOOLBOX_DISPLAY', 1);

function jaGenerateLargeBox($smallBox){
	$toolboxLarger = $smallBox;
	//facebookshare
	$toolboxLarger = str_replace("button_count", "box_count", $toolboxLarger);
	//twitter
	$toolboxLarger = str_replace("horizontal", "vertical", $toolboxLarger);
	return $toolboxLarger;
}
?>
<script type="text/javascript">
/*<![CDATA[*/
var ja_social_toolbox_timer;
<?php	 	 	 	 if(T3Common::isRTL()): ?>
var ja_social_toolbox_direction = 'rtl';
<?php	 	 	 	 else: ?>
var ja_social_toolbox_direction = 'ltr';
<?php	 	 	 	 endif; ?>

function ja_social_toolbox_display() {
	var size = $(window).getSize();
	if(size.size.x <= 1080) {
		$$('div.ja-social-toolbox-small').setStyle('display', 'block');
		$$('div.ja-social-toolbox-large').setStyle('display', 'none');
	} else {
		$$('div.ja-social-toolbox-small').setStyle('display', 'none');
		$$('div.ja-social-toolbox-large').setStyle('display', 'block');
	}
}

window.addEvent('domready', function() {
	ja_social_toolbox_display();
});

window.addEvent('resize', function(){
  $clear(ja_social_toolbox_timer);
  ja_social_toolbox_timer = (function(){
    ja_social_toolbox_display();
  }).delay(50);
});

function jaGetScrollXY() {
	var scrOfX = 0, scrOfY = 0;
	if( typeof( window.pageYOffset ) == 'number' ) {
		//Netscape compliant
		scrOfY = window.pageYOffset;
		scrOfX = window.pageXOffset;
	} else if( document.body && ( document.body.scrollLeft || document.body.scrollTop ) ) {
		//DOM compliant
		scrOfY = document.body.scrollTop;
		scrOfX = document.body.scrollLeft;
	} else if( document.documentElement && ( document.documentElement.scrollLeft || document.documentElement.scrollTop ) ) {
		//IE6 standards compliant mode
		scrOfY = document.documentElement.scrollTop;
		scrOfX = document.documentElement.scrollLeft;
	}
	return {'x': scrOfX, 'y': scrOfY };
}

window.addEvent('scroll', function(e){
	if($('ja-social-toolbox-float')) {
		var pos = $('ja-social-toolbox-float').getPosition();
		
		if($('ja-social-toolbox-float').getProperty('offsetY') == null) {
			$('ja-social-toolbox-float').setProperty('offsetY', pos.y );
		}
		var elementY = $('ja-social-toolbox-float').getProperty('offsetY').toInt();
		
		//alert(window.pageYOffset + ':' + elementY);
		var offset = jaGetScrollXY();
		var offsetY = offset.y;
		if(offsetY > elementY) {
			var left = 0;
			$$('#ja-container div.main').each(function(ele){
				left = ele.getCoordinates().width;	
			});
			
			var wWidth = window.getWidth();
			if(ja_social_toolbox_direction == 'rtl') {
				left = left + (wWidth - left) / 2;
			} else {
				var coor = $('ja-social-toolbox-float').getCoordinates();
				
				left = ((wWidth - left)/2 - coor.width);
			}
			
			$('ja-social-toolbox-float').setStyles({
				'left': left,
				'right': 'auto',
				'position': 'fixed',
				'top': '20px'
			});
		} else {
			$('ja-social-toolbox-float').setStyles({
				'left': '',
				'right': '',
				'position': '',
				'top': ''
			});
		}
	}
});

/*]]>*/
</script>
<?php	 	 	 	 endif; ?>