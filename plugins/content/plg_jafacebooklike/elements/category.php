<?php	 	 	 	 
/**
 * JA Bookmark Plugin is a module using for Display icons for your online social networking sites.
 *
 * @version 	1.0.0.0
 * @author 		joomlart <@email: webmaster@joomlart.com>
 * @link		http://www.joomlart.com
 * @copyright 	Copyright (C) August - 2009, J.O.O.M Solutions Co., Ltd. All Rights Reserved.
 * @license 	http://www.joomlart.com GNU/GPL
 */
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die( 'Restricted access' );

class JElementCategory extends JElement
{
	/*
	 * Category name
	 *
	 * @access	protected
	 * @var		string
	 */
	var	$_name = 'Category';
	
	var $_controlName = '';
	/**
	 * fetch Element 
	 */
	function fetchElement($name, $value, &$node, $control_name){
		$db = &JFactory::getDBO();
		$query = '
			SELECT 
				c.section,
				s.title AS section_title,
				c.id AS cat_id,
				c.title AS cat_title 
			FROM #__sections AS s
			INNER JOIN #__categories c ON c.section = s.id
			WHERE s.published=1
			AND c.published = 1
			ORDER BY c.section, c.title
			';
		$db->setQuery( $query );
		$cats = $db->loadObjectList();
		$HTMLCats=array();
		$HTMLCats[0]->id = '';
		$HTMLCats[0]->title = JText::_("ALL CATEGORY");
		$section_id = 0;
		foreach ($cats as $cat) {
			if($section_id != $cat->section) {
				$section_id = $cat->section;
				
				$cat->id = $cat->section;
				$cat->title = $cat->section_title;
				$optgroup = JHTML::_('select.optgroup', $cat->title, 'id', 'title');
				array_push($HTMLCats, $optgroup);
			}
			$cat->id = $cat->cat_id;
			$cat->title = $cat->cat_title;
			array_push($HTMLCats, $cat);
		}
		return JHTML::_('select.genericlist',  $HTMLCats, ''.$control_name.'['.$name.'][]', 'class="inputbox" style="width:95%;" multiple="multiple" size="10"', 'id', 'title', $value );
	}
}
?>