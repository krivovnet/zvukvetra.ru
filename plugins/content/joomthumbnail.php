<?php	 	 	 	
/**
 * JoomThumbnail -  all gallery in one plugin.
 *
 * @version 2.0
 * @author Dmitriy Kupriyanov (ageent.ua@gmail.com)
 * @copyright (C) 2010 by Dmitriy Kupriyanov (http://sitetranslation.org)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 **/
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
jimport('joomla.plugin.plugin');

class plgContentJoomthumbnail extends JPlugin {
    
    var $options;
    var $gallery;
    
    function plgContentJoomthumbnail(& $subject, $params, $special = 0) {
        parent::__construct($subject, $params);
        $plugin = &JPluginHelper::getPlugin('content', 'joomthumbnail');
        $params_now = new JParameter($plugin->params);
        
        $path = $params_now->get("full_pach");
        if(empty($path)) {
            $path_to_small_img = JPATH_ROOT."/images/stories/";
            $path_to_small_url = JURI::root()."images/stories/";
        } else {
            $path_to_small_img = JPATH_ROOT."/".$path;
            $path_to_small_url = JURI::root().$path;
        }
        
        $this->options = array(
                    'gallery' => $params_now->get("gallery","highslide"),
                    'adress_site' => JURI::base(),
                    'path_to_full_img' => JPATH_BASE,
                    'path_to_small_img' => $path_to_small_img,
                    'path_to_small_url' => $path_to_small_url,
                    'width'   => $params_now->get("width", ""),
                    'height'  => $params_now->get("height", ""),
                    'method'  => $params_now->get("method", "THUMBNAIL_METHOD_SCALE_MIN"),
                    'halign'  => $params_now->get("halign", "HUMBNAIL_ALIGN_CENTER"),
                    'valign'  => $params_now->get("valign", "HUMBNAIL_ALIGN_CENTER"),
                    'percentage' => 10,
                    'use_risaze_img' => $params_now->get("use_risaze_img", "yes"),
                    'include_jquery' => $params_now->get("include_jquery", "yes"),
                    'i_want_img' => $params_now->get('i_want_img',"not"),
                    'link_highslide' => $params_now->get('link_highslide',"yes"),
                    'border_tracings' => $params_now->get('border_tracings',"not")
        );
        $this->gallery = new All_gallery($this->options);
    }
    
    function onPrepareContent(&$article, &$params, $limitstart=0) {
        $article->text=$this->gallery->get_content($article->text,$article->id);
    }
    
    function onBeforeContentSave(&$article) {
            $this->gallery->get_clear_content($article->introtext,$article->id);
            $this->gallery->get_clear_content($article->fulltext,$article->id);
        return  $article;
    }
}