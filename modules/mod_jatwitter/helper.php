<?php	 	 	 	
/*
# ------------------------------------------------------------------------
# JA Twitter module for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/
// no direct access
defined ( '_JEXEC' ) or die ( 'Restricted access' );
/**
 * modJaTwitterHelper  class.
 */
class modJaTwitterHelper {
	
	/**
	 * @var JATwitter $jaTwitter
	 *
	 * @access public.
	 */
	var $jaTwitter = null;
	
	/**
	 * @var boolean $isCache
	 *
	 * @access public.
	 */
	var $isCache = false;
	
	/**
	 * @var integer $cacheTimeLife
	 *
	 * @access public.
	 */
	
	var $cacheTimeLife = 30;
	
	/**
	 * constructor
	 */
	function modJaTwitterHelper($username = '', $password = '') {
		$this->jaTwitter = new JATwitter ();
		// set username and password using for oAuth
	//$this->jaTwitter = $this->jaTwitter->setAuth( $username, $password );		
	}
	
	/**
	 * set options using for cache data
	 *
	 * @param boolean enable $use equal true
	 */
	function setCache($use = true, $timeLife = 30) {
		$this->isCache = $use;
		$this->cacheTimeLife = $timeLife;
	}
	
	/**
	 * get twitter's data base on method call, and process get and store data in  cache file
	 *
	 * @param string $twitterMethod api twitter method (@see http://apiwiki.twitter.com/Twitter-API-Documentation)
	 * @param string $screenName
	 * @param integer $count
	 * @param integer $overrideCacheTime
	 * @return array.
	 */
	function getTwitter($twitterMethod = 'show', $screenName, $count = 10, $overrideCacheTime = false) {
		// check data valid 
		if ($screenName == '') {
			return false;
		}
		
		$this->jaTwitter->setScreenName ( $screenName );
		// if enable cache data
		if ($this->isCache) {
			$cache = & JFactory::getCache ();
			$cache->setCaching ( true );
			if ($overrideCacheTime) {
				$cache->setLifeTime ( $overrideCacheTime * 60 );
			} else {
				$cache->setLifeTime ( $this->cacheTimeLife * 60 );
			}
			$data = $cache->get ( array ($this->jaTwitter, 'getTweets' ), array ($twitterMethod, $count ) );
		
		} else {
			$data = $this->jaTwitter->getTweets ( $twitterMethod, $count );
		}
		return $data;
	}
	
	/**
	 * add hyper link......
	 *
	 * @var string $description
	 * @return string.
	 */
	function convert($description) {
		
		$description = preg_replace ( "#(^|[\n ])@([^ \"\t\n\r<]*)#ise", "'\\1<a href=\"http://www.twitter.com/\\2\" >@\\2</a>'", $description );
		$description = preg_replace ( "#(^|[\n ])([\w]+?://[\w]+[^ \"\n\r\t<]*)#ise", "'\\1<a href=\"\\2\" >\\2</a>'", $description );
		$description = preg_replace ( "#(^|[\n ])((www|ftp)\.[^ \"\t\n\r<]*)#ise", "'\\1<a href=\"http://\\2\" >\\2</a>'", $description );
		$description = preg_replace ( "#(^|[\n ])\#([^ \"\t\n\r<:]*)#ise", "'\\1<a target=\"_blank\" href=\"http://twitter.com/search?q=#\\2\" >\\2</a>'", $description );
		$description = str_replace ( '&amp;', '&', $description );
		$description = str_replace ( '&', '&amp;', $description );
		return $description;
	}
	
	/**
	 * convert twitter's data to friendly date.
	 *
	 * @param string $createAt.
	 * @return string.
	 */
	function getDate($createdAt) {
		
		$now = & JFactory::getDate ();
		$createdAt = preg_replace ( "(\+\d{4}\s+)", "", $createdAt );
		
		$date = & JFactory::getDate ( strtotime ( $createdAt ) + ($now->toUnix () - time ()) );
		
		$diff = $now->toUnix () - $date->toUnix ();
		
		if ($diff < 60) {
			$createdDate = JText::_ ( 'LESS THAN A MINUTE AGO' );
		} elseif ($diff < 120) {
			$createdDate = JText::_ ( 'ABOUT A MINUTE AGO' );
		} elseif ($diff < (45 * 60)) {
			$createdDate = JText::sprintf ( '%s MINUTES AGO', round ( $diff / 60 ) );
		} elseif ($diff < (90 * 60)) {
			$createdDate = JText::_ ( 'ABOUT AN HOUR AGO' );
		} elseif ($diff < (24 * 3600)) {
			$createdDate = JText::sprintf ( 'ABOUT %s HOURS AGO', round ( $diff / 3600 ) );
		} else {
			$createdDate = JHTML::_ ( 'date', $date->toUnix (), JText::_ ( 'DATE_FORMAT_LC2' ) );
		}
		
		return $createdDate;
	}
	
	/**
	 * load css and js file.
	 *
	 * @param JParameter $params
	 * @param stdClass contain module information.
	 */
	function loadFiles($params, $module) {
		//if( $params->get('load_css_file') ) {
		global $mainframe;
		JHTML::stylesheet ( 'style.css', 'modules/' . $module . '/assets/' );
		if (is_file ( JPATH_SITE . DS . 'templates' . DS . $mainframe->getTemplate () . DS . 'css' . DS . $module . ".css" ))
			JHTML::stylesheet ( $module . ".css", 'templates/' . $mainframe->getTemplate () . '/css/' );
		//}
	}
	
	/**
	 * get list from RSS resouce, it's legacy help to run old version
	 *
	 * @params JParam $params
	 * @return Object xml. or boolean 
	 */
	function getList($params) {
		
		if (trim ( $params->get ( 'taccount' ) ) == '') {
			return false;
		}
		$rssUrl = "http://twitter.com/statuses/user_timeline/" . $params->get ( 'taccount' ) . ".rss?count=" . $params->get ( 'show_limit' );
		
		//  get RSS parsed object
		$options = array ();
		$options ['rssUrl'] = $rssUrl;
		if ($params->get ( 'enable_cache' )) {
			$options ['cache_time'] = $params->get ( 'cache_time' );
			$options ['cache_time'] *= 60;
		} else {
			$options ['cache_time'] = null;
		}
		
		$rssDoc = & JFactory::getXMLparser ( 'RSS', $options );
		
		if ($rssDoc != false) {
			$items = $rssDoc->get_items ();
			return $items;
		} else {
			return false;
		}
	}
}
?>