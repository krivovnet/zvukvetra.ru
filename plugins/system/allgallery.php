<?php	 	 	 	
/**
 * JoomThumbnail -  all gallery in one plugin.
 *
 * @version 2.0
 * @author Dmitriy Kupriyanov (ageent.ua@gmail.com)
 * @copyright (C) 2010 by Dmitriy Kupriyanov (http://sitetranslation.org)
 * @license GNU/GPL: http://www.gnu.org/copyleft/gpl.html
 **/
 
defined( '_JEXEC' ) or die( 'Restricted access' );
 
include_once(JPATH_PLUGINS.DS."system".DS."allgallery".DS."joomthumbnail.php");