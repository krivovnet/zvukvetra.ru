<?php
/*
# ------------------------------------------------------------------------
# JA Disqus and Debate comment for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/
//Disqus config
$subdomain      = $this->plgParams->get('provider-disqus-subdomain');
$devmode        = $this->plgParams->get('provider-disqus-devmode',0);

$devcode = "";
if ($devmode) 
	$devcode = "var disqus_developer = \"1\";";

?>
<script type='text/javascript'>
//<![CDATA[ 
	<?php echo $devcode; ?>
	var disqus_shortname = '<?php echo $subdomain; ?>';
	(function () {
	  var s = document.createElement('script'); s.async = true;
	  s.src = 'http://disqus.com/forums/<?php echo $subdomain; ?>/count.js?v=<?php echo time(); ?>';
	  (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(s);
	}());
//]]> 
</script>