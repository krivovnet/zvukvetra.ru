<?php	 	 	 	
if ( !function_exists('sys_get_temp_dir') ) {
	function sys_get_temp_dir() {
		// Try to get from environment variable
		if ( !empty($_ENV['TMP']) ) {
			return realpath( $_ENV['TMP'] );
		} elseif ( !empty($_ENV['TMPDIR']) ) {
			return realpath( $_ENV['TMPDIR'] );
		} elseif ( !empty($_ENV['TEMP']) ) {
			return realpath( $_ENV['TEMP'] );
		} else {
			// Try to use system's temporary directory
			// as random name shouldn't exist
			$temp_file = tempnam( md5(uniqid(rand(), TRUE)), '' );
			if ( $temp_file ) {
				$temp_dir = realpath( dirname($temp_file) );
				unlink( $temp_file );
				return $temp_dir;
			} else {
				return null;
			}
		}
	}
}
?>