<?php 
/*
# ------------------------------------------------------------------------
# JA Twitter module for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/ 
// no direct access
defined('_JEXEC') or die('Restricted access');	
?>
<div id="ja_twitter_div" class="content">
	<?php if($showtextheading == "1"){ ?>
		<h4 class="twitter-title"><?php echo $headingtext;?></h4>
	<?php } ?>
	<?php if( $displayitem && $twitters != false )  { ?>
		<ul id="twitter_update_list">
		<?php foreach( $twitters as $item ) { ?>
			<li><span><?php echo modJaTwitterHelper::convert( preg_replace( "(^".$taccount.":)" , '',$item->get_description()) );?></span>&nbsp;&nbsp;<a href="<?php echo $item->get_link();?>"><?php echo round( (strtotime( date('D, j M Y') ) - strtotime( $item->get_date('D, j M Y') )) /(24*60*60), 0);?> <?php echo JText::_('days ago')?></a></li>
		<?php } ?>
		</ul>
	<?php } ?>
	<?php if ( $showfollowlink == "1" ){ ?>
		<center>
			<script src='http://platform.twitter.com/anywhere.js?id=<?php echo $apikey?>' type='text/javascript'></script>
			<div id="anywhere-block-follow-button-<?php echo $module->id?>"></div>
			<script type="text/javascript">
				twttr.anywhere(function(twitter) {
			  	twitter('#anywhere-block-follow-button-<?php echo $module->id?>').followButton("<?php echo $screenName?>");
					})
			</script>
		</center>
	<?php }?>
</div>
