<?php 
/*
# ------------------------------------------------------------------------
# JA Twitter module for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/ 
// no direct access
defined('_JEXEC') or die('Restricted access');
?>
<div id="ja_twitter_div" class="content">
	<?php if($showtextheading == "1"){ ?>
		<h4 class="twitter-title"><?php echo $headingtext;?></h4>
	<?php } ?>
	<?php if($displayitem) { ?>
	<div id="twitter_update_list">
		<img src="<?php echo JURI::base();?>modules/mod_jatwitter/assets/images/mootree_loader.gif" alt="Loader" />
	</div>
	<?php } ?>
	<?php if ( $showfollowlink == "1" ){ ?>
		<center>
		<script src='http://platform.twitter.com/anywhere.js?id=<?php echo $apikey?>' type='text/javascript'></script>
<div id="anywhere-block-follow-button"></div>
  <script type="text/javascript">
  	twttr.anywhere(function(twitter) {
    	twitter('#anywhere-block-follow-button').followButton("<?php echo $screenName?>");
  		})
  </script>
  </center>
	<?php }  ?>
</div>
<?php if($displayitem) { ?>
<script type="text/javascript">

	
	var myVerticalSlide = new Fx.Slide('twitter_update_list');
	
	var myAjax = new Ajax( '<?php echo JURI::base();?>modules/mod_jatwitter/twitter.php',
						 { method: 'post',
						   data: {username:'<?php echo $taccount;?>','count':<?php echo $show_limit;?>, modid: '<?php echo $module->id;?>'},
						   update:  $('twitter_update_list'),
						   onComplete:function( html ){
				}
	}).request();
</script>
<?php } ?>
