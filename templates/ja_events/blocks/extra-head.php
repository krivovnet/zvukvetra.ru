<style type="text/css">
	#ja-mainnav .main-inner2 {width: <?php	 	 	 	 echo $this->getColumnWidth('mw') ?>%;}
</style>

<!-- width for left, right banner -->
<script type="text/javascript">
	window.addEvent ('load', function () {
		var w = $('ja-wrapper').offsetWidth;
		if ($('ja-container') && $('ja-container').getElement('.main')) w -= $('ja-container').getElement('.main').offsetWidth;
		w /= 2;
		if ($('ja-left-banner')) $('ja-left-banner').setStyle ('width', w);
		if ($('ja-right-banner')) $('ja-right-banner').setStyle ('width', w);
		//height for top-banner
		if ($('ja-header') && $('ja-top-banner')) $('ja-top-banner').setStyle ('height', $('ja-header').offsetHeight);
	});
</script>