<?php
/*
# ------------------------------------------------------------------------
# JA Disqus and Debate comment for joomla 1.5
# ------------------------------------------------------------------------
# Copyright (C) 2004-2010 JoomlArt.com. All Rights Reserved.
# @license - PHP files are GNU/GPL V2. CSS / JS are Copyrighted Commercial,
# bound by Proprietary License of JoomlArt. For details on licensing, 
# Please Read Terms of Use at http://www.joomlart.com/terms_of_use.html.
# Author: JoomlArt.com
# Websites:  http://www.joomlart.com -  http://www.joomlancers.com
# Redistribution, Modification or Re-licensing of this file in part of full, 
# is bound by the License applied. 
# ------------------------------------------------------------------------
*/
//Disqus config
$subdomain      = $this->plgParams->get('provider-disqus-subdomain');
$devmode        = $this->plgParams->get('provider-disqus-devmode',0);

$devcode = "";
if ($devmode) 
	$devcode = "var disqus_developer = \"1\";";

$sefUrl = $this->_sefurl;
if ($this->isComment == false) {
?>
	<div class="jacomment-count">
    <a class="jacomment-counter" href="<?php echo $this->_sefurl; ?>#disqus_thread" onclick="location.href='<?php echo $this->_url; ?>#disqus_thread'; return false;" title="">
	<?php echo JText::_("COMMENTS"); ?>
    </a>
    </div>
<?php 
} else {
	if(!defined("JA_EMBEDED_DISQUS_FORM")):
		define("JA_EMBEDED_DISQUS_FORM", 1);
?>
    <div id="disqus_thread"></div>
	<script type="text/javascript">
      /**
        * var disqus_identifier; [Optional but recommended: Define a unique identifier (e.g. post id or slug) for this thread] 
        */
		<?php echo $devcode?>
		
		var disqus_url= "<?php echo $sefUrl; ?>";
		var disqus_identifier = "<?php echo $this->_postid ?>";
      (function() {
       var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
       dsq.src = 'http://<?php echo $subdomain; ?>.disqus.com/embed.js';
       (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
      })();
    </script>
    <noscript>Please enable JavaScript to view the <a href="http://disqus.com/?ref_noscript=<?php echo $subdomain; ?>">comments powered by Disqus.</a></noscript>
	<a href="http://disqus.com" class="dq-powered"><?php echo JText::_("BLOG COMMENTS POWERED BY DISQUS"); ?></a>
<?php 
	endif;
} 
?>