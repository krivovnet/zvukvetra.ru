<table id="ja-info" cellpadding="0" cellspacing="0"  width="100%">	
	<tr class="level1">
		<td class="ja-block-head">
			<h4  id="ja-head-additionalinformation" class="block-head block-head-additionalinformation open" rel="1" >
				<span class="block-setting" ><?php	 	 	 	 echo JText::_('Additional Information')?></span> 
				<span class="icon-help editlinktip hasTip" title="<?php	 	 	 	 echo JText::_('Additional Information')?>::<?php	 	 	 	 echo sprintf(JText::_('Additional Information desc'), strtoupper($template))?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>
				<a class="toggle-btn open" title="<?php	 	 	 	 echo JText::_('Expand all')?>" onclick="showRegion('ja-head-additionalinformation', 'level1'); return false;"><?php	 	 	 	 echo JText::_('Expand all')?></a>
				<a class="toggle-btn close" title="<?php	 	 	 	 echo JText::_('Collapse all')?>" onclick="hideRegion('ja-head-additionalinformation', 'level1'); return false;"><?php	 	 	 	 echo JText::_('Collapse all')?></a>
			</h4>
			
			<div style="text-align: left; padding-right: 4px;">
				<ul class="ja-quicklinks clearfix">
					<li><span>1</span><a href="http://www.joomlart.com/forums/showthread.php?47403-JA-Events-Userguide&p=202345#post202345" target="_blank" title="Template userguide">Template userguide</a></li>
					<li><span>2</span><a href="http://www.joomlart.com/demo/#templates.joomlart.com/ja_events" target="_blank" title="Template Live Demo">Template Live Demo</a></li>
					<li><span>3</span><a href="http://www.joomlart.com/forums/forumdisplay.php?248-JA-Events" target="_blank" title="Report Bug">Report Bug / Forum</a></li>
					<li><span>4</span><a href="http://update.joomlart.com/#products.list/template/JA%20Events%20template/" target="_blank" title="Check Version">Check Version</a></li>
					<li><span>5</span><a href="http://www.joomlart.com/demo/#templates.joomlart.com/ja_events/index.php?option=com_content&view=article&id=55&Itemid=59&tab=0" target="_blank" title="Supported Typography">Supported Typography</a></li>
					<li><span>6</span><a href="http://www.joomlart.com/joomla/templates/ja-events" target="_blank" title="JA Events - Template for Joomla! Events ">JA Events - Template Info </a></li>
				</ul>
			</div>
		</td>
    </tr>
</table>
<div id="ja-info-more">
	
</div>	


<script type="text/javascript">
	function updateAdditionalInfo(){
		$('link-update').setStyle('display', 'none');
		$('link-update-loading').setStyle('display', 'block');
		new Ajax('?jat3type=plugin&jat3action=updateAdditionalInfo&template=<?php	 	 	 	 echo $template?>', {method: 'post', onComplete: inserAdditionalInfo}).request();		
	}	

	function inserAdditionalInfo(text){
		$('link-update').setStyle('display', 'block');
		$('link-update-loading').setStyle('display', 'none');
		$('ja-info-more').innerHTML = text;
	}
</script>

